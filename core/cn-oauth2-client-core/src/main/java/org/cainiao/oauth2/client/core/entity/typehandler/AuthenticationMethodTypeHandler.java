package org.cainiao.oauth2.client.core.entity.typehandler;

import com.baomidou.mybatisplus.extension.handlers.AbstractJsonTypeHandler;
import org.cainiao.oauth2.client.util.OAuth2Util;
import org.springframework.security.oauth2.core.AuthenticationMethod;
import org.springframework.util.StringUtils;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class AuthenticationMethodTypeHandler extends AbstractJsonTypeHandler<AuthenticationMethod> {

    public AuthenticationMethodTypeHandler() {
        super(AuthenticationMethod.class);
    }

    @Override
    public AuthenticationMethod parse(String jsonString) {
        return StringUtils.hasText(jsonString) ? OAuth2Util.resolveAuthenticationMethod(jsonString) : null;
    }

    @Override
    public String toJson(AuthenticationMethod authenticationMethod) {
        return authenticationMethod == null ? null : authenticationMethod.getValue();
    }
}
