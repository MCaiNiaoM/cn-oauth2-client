package org.cainiao.oauth2.client.core.entity.typehandler;

import com.baomidou.mybatisplus.extension.handlers.AbstractJsonTypeHandler;
import org.cainiao.oauth2.client.util.OAuth2Util;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.util.StringUtils;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class ClientAuthenticationMethodTypeHandler extends AbstractJsonTypeHandler<ClientAuthenticationMethod> {

    public ClientAuthenticationMethodTypeHandler() {
        super(ClientAuthenticationMethod.class);
    }

    @Override
    public ClientAuthenticationMethod parse(String jsonString) {
        return StringUtils.hasText(jsonString) ? OAuth2Util.resolveClientAuthenticationMethod(jsonString) : null;
    }

    @Override
    public String toJson(ClientAuthenticationMethod clientAuthenticationMethod) {
        return clientAuthenticationMethod == null ? null : clientAuthenticationMethod.getValue();
    }
}
