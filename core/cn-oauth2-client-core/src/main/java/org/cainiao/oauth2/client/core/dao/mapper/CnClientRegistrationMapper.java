package org.cainiao.oauth2.client.core.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.cainiao.oauth2.client.core.entity.CnClientRegistration;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public interface CnClientRegistrationMapper extends BaseMapper<CnClientRegistration> {
}
