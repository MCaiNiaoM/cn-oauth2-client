package org.cainiao.oauth2.client.core.component.requestcache;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

/**
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class GetHtmlHttpServletRequest extends HttpServletRequestWrapper {

    private final String requestURI;

    public GetHtmlHttpServletRequest(HttpServletRequest request, String requestURI) {
        super(request);
        this.requestURI = requestURI;
    }

    @Override
    public String getServletPath() {
        return requestURI;
    }

    @Override
    public String getRequestURI() {
        return requestURI;
    }

    @Override
    public StringBuffer getRequestURL() {
        StringBuffer stringBuffer = new StringBuffer(getScheme());
        stringBuffer.append("://").append(getServerName()).append(getRequestURI());
        return stringBuffer;
    }

    @Override
    public String getQueryString() {
        return null;
    }

    @Override
    public String getMethod() {
        return HttpMethod.GET.name();
    }

    @Override
    public String getHeader(String name) {
        if (HttpHeaders.ACCEPT.equalsIgnoreCase(name)) {
            return MediaType.TEXT_HTML_VALUE;
        }
        return super.getHeader(name);
    }
}
