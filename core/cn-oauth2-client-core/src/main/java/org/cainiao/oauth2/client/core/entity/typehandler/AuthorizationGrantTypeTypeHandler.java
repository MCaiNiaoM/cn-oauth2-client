package org.cainiao.oauth2.client.core.entity.typehandler;

import com.baomidou.mybatisplus.extension.handlers.AbstractJsonTypeHandler;
import org.cainiao.oauth2.client.util.OAuth2Util;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.util.StringUtils;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class AuthorizationGrantTypeTypeHandler extends AbstractJsonTypeHandler<AuthorizationGrantType> {

    public AuthorizationGrantTypeTypeHandler() {
        super(AuthorizationGrantType.class);
    }

    @Override
    public AuthorizationGrantType parse(String jsonString) {
        return StringUtils.hasText(jsonString) ? OAuth2Util.resolveAuthorizationGrantType(jsonString) : null;
    }

    @Override
    public String toJson(AuthorizationGrantType authorizationGrantType) {
        return authorizationGrantType == null ? null : authorizationGrantType.getValue();
    }
}
