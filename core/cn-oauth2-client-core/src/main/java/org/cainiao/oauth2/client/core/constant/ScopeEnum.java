package org.cainiao.oauth2.client.core.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用 @HasScope 注解来标注需要的 Scope 时，可以使用的类型安全的 Scope 枚举<br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@AllArgsConstructor
@Getter
public enum ScopeEnum {

    LARK("飞书"), GITEE("Gitee");

    final String description;
}
