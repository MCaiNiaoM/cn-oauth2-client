package org.cainiao.oauth2.client.core.component.requestcache;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@RequiredArgsConstructor
public class CustomRequestCache extends HttpSessionRequestCache {

    /**
     * 登录前缓存请求以便在登录成功后重放<br />
     * 在前后端分离的情况下，请求都是 AJAX 请求，不应该被重放<br />
     * 因此通过自定义 RequestCache，重放请求始终为网站的中间页<br />
     * 由前端在重定向到授权服务器之前缓存当时的路由以便重放<br />
     * TODO 可配置中间页地址，试验这里是否可以加锚点 # 作为哈希类型的前端路由
     *
     * @param request  the request to be stored
     * @param response HttpServletResponse
     */
    @Override
    public void saveRequest(HttpServletRequest request, HttpServletResponse response) {
        super.saveRequest(new GetHtmlHttpServletRequest(request, ""), response);
    }
}
