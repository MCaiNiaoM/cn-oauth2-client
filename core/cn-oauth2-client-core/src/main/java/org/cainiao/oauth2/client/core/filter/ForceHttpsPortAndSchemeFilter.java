package org.cainiao.oauth2.client.core.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

import java.io.IOException;

/**
 * OAuth2 的 redirectUrl 使用的是 HTTPS，但应用程序是在反向代理后边的，因此在应用程中获取到的当前请求的 schema 是 HTTP，而不是 HTTPS<br />
 * 目前发现在两个地方会存在这个问题<br />
 * 1）OAuth2 客户端，在没有登录时，请求任意 AJAX API<br />
 * 2）code 换 access token<br />
 * 由于第一种情况是任意 AJAX 请求，因此不能只处理情况二的端点，所有请求都要处理<br />
 * 通常生产用 HTTPS，开发或测试可能用 HTTP，因此默认为强制返回 HTTPS，但可以通过属性配置来关闭这个功能<br />
 * 只要确定某个环境一定是 HTTPS，则可以开启这个强制转换，这样就不用再担心反向代理造成的错误 scheme 带来的其它潜在问题
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class ForceHttpsPortAndSchemeFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest httpRequest) {
            chain.doFilter(new HttpServletRequestWrapper(httpRequest) {
                @Override
                public int getServerPort() {
                    return 443;
                }
                @Override
                public String getScheme() {
                    return "https";
                }
            }, response);
        }
    }
}
