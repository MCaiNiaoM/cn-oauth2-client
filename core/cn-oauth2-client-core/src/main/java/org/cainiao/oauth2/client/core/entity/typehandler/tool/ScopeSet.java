package org.cainiao.oauth2.client.core.entity.typehandler.tool;

import java.io.Serial;
import java.util.HashSet;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class ScopeSet extends HashSet<String> {

    @Serial
    private static final long serialVersionUID = -2617107113041546617L;
}
