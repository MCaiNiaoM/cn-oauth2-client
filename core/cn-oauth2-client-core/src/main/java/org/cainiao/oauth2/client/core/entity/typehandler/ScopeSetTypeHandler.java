package org.cainiao.oauth2.client.core.entity.typehandler;

import com.baomidou.mybatisplus.extension.handlers.AbstractJsonTypeHandler;
import com.fasterxml.jackson.core.type.TypeReference;
import org.cainiao.common.exception.BusinessException;
import org.cainiao.oauth2.client.core.entity.typehandler.tool.ScopeSet;
import org.cainiao.oauth2.client.core.util.JsonUtil;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class ScopeSetTypeHandler extends AbstractJsonTypeHandler<ScopeSet> {

    public ScopeSetTypeHandler() {
        super(ScopeSet.class);
    }

    @Override
    public ScopeSet parse(String jsonString) {
        try {
            return JsonUtil.parseJson(jsonString, new TypeReference<>() {
            });
        } catch (Exception e) {
            throw new BusinessException("Failed to convert JSON to Set<String>", e);
        }
    }

    @Override
    public String toJson(ScopeSet set) {
        try {
            return JsonUtil.toJsonString(set);
        } catch (Exception e) {
            throw new BusinessException("Failed to convert Set<String> to JSON", e);
        }
    }
}
