package org.cainiao.oauth2.client.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@ConfigurationProperties(prefix = "cn.oauth2.client")
@Data
public class CNOAuth2ClientProperties {

    private boolean enable = true;

    /**
     * 外部访问的地址加的前缀，内部 UrlMatcher 匹配不加这个前缀
     * 具体的：除了 DefaultOAuth2AuthorizationRequestResolver 其它地方会加上这个前缀
     * 用于会被反向代理去除的前缀，即外部访问的地址必须加上这个前缀，否则反向代理无法路由到上游服务
     * 但在服务内部接收到的 uri 已经没有这个前缀了，已经被反向代理删除
     */
    private String cgiUriPrefix = "/cgi";

    /**
     * 内部外部 url 都会加的前缀
     * 通常在使用 uri 来区分服务时，这个前缀配置为服务部分的 uri
     */
    private String authorizationRequestBaseUriPrefix = "";

    private boolean forceHttps = true;

    private String authorizationServerBaseUrl = "http://127.0.0.1:9000";

    private String loginFormUrlPrefix;
}
