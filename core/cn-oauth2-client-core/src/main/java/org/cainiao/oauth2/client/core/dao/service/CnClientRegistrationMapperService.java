package org.cainiao.oauth2.client.core.dao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.cainiao.oauth2.client.core.dao.mapper.CnClientRegistrationMapper;
import org.cainiao.oauth2.client.core.entity.CnClientRegistration;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Service
public class CnClientRegistrationMapperService
    extends ServiceImpl<CnClientRegistrationMapper, CnClientRegistration> implements IService<CnClientRegistration> {

    public CnClientRegistration findByRegistrationId(String registrationId) {
        return lambdaQuery().eq(CnClientRegistration::getRegistrationId, registrationId).one();
    }

    public List<CnClientRegistration> findByRegistrationIdIn(List<String> registrationIds) {
        return lambdaQuery().in(CnClientRegistration::getRegistrationId, registrationIds).list();
    }
}
