package org.cainiao.oauth2.client.core.entity.typehandler;

import com.baomidou.mybatisplus.extension.handlers.AbstractJsonTypeHandler;
import com.fasterxml.jackson.core.type.TypeReference;
import org.cainiao.common.exception.BusinessException;
import org.cainiao.oauth2.client.core.entity.typehandler.tool.ConfigurationMetadataMap;
import org.cainiao.oauth2.client.core.util.JsonUtil;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class ConfigurationMetadataMapTypeHandler extends AbstractJsonTypeHandler<ConfigurationMetadataMap> {

    public ConfigurationMetadataMapTypeHandler() {
        super(ConfigurationMetadataMap.class);
    }

    @Override
    public ConfigurationMetadataMap parse(String jsonString) {
        try {
            return JsonUtil.parseJson(jsonString, new TypeReference<>() {
            });
        } catch (Exception e) {
            throw new BusinessException("Failed to convert JSON to Map<String, Object>", e);
        }
    }

    @Override
    public String toJson(ConfigurationMetadataMap map) {
        try {
            return JsonUtil.toJsonString(map);
        } catch (Exception e) {
            throw new BusinessException("Failed to convert Map<String, Object> to JSON", e);
        }
    }
}
