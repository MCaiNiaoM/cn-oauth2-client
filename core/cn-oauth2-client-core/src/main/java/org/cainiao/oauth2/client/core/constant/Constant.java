package org.cainiao.oauth2.client.core.constant;

import org.springframework.security.oauth2.core.oidc.OidcScopes;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class Constant {

    public static final Set<String> COMMON_SCOPES = new LinkedHashSet<>(Arrays.asList(OidcScopes.OPENID, OidcScopes.PROFILE, OidcScopes.EMAIL));
}
