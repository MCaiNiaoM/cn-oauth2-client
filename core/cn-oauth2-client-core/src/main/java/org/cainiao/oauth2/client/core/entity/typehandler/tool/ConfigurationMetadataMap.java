package org.cainiao.oauth2.client.core.entity.typehandler.tool;

import java.io.Serial;
import java.util.HashMap;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public class ConfigurationMetadataMap extends HashMap<String, Object> {

    @Serial
    private static final long serialVersionUID = -2491604114070467411L;
}
